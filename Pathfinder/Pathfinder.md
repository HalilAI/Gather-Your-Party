# Pathfinder
## Title
GYP | Pathfinder - v0.5

## Author
HalilAI

## Description
Welcome to Golarion. 
Enjoy 550 World Info Entries, or ~34,000 words of setting and backstory, in addition to some pre-written adventures to set you up.
Don't talk to Asmodeus, make Beholders dream of dreaming Beholders, or tickle Rovagug.

Gather Your Party is a cooperative world creation environment. You can find this scenario there at https://github.com/HalilCan/Gather-Your-Party.

## Prompt
At the very least, keep track of the motivations of your core characters in Memory/Remember. Then comes increasingly optional info: abilities, appearance, apparel...

Welcome.
If you have World Info or Scripting suggestions, please post them in comments or in the AID Discord to Halil #3541. Alternatively you could make those changes yourself or create entirely new Pathfinder-based scenarios on the Gather Your Party repository.

You could literally go through Ultimate Campaign, pick and choose your background, (or pick an adventure module) and plop it onto this base module as a new scenario option, and it works!
Or, you can choose one of the preset adventures I created for you down below!
Have fun!

## Memory

## Author's Note
This magical world is filled with wonders; use a descriptive writing style.

## Quests

## Music Theme
None

## Tags
pathfinder, rpg, fantasy, dungeons and dragons, dnd, multiplayer, adventure, gather your party, gyp, worlds

## Properties
* NSFW: No
* Published: Yes
* Safe Mode: No
* 3rd Person Only: No

## Mode
Creative
