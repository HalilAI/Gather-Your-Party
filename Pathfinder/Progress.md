# World Info Progress
## What's in the World Info file:
* Significant information on Absalom, its districts, some of its notable citizens, and its primary attractions. Too many to easily list here. Pls help.
* Primary deities of Golarion
* Continents and general geography of Golarion
* Information about Arcadia and its inhabitants
* General information on Azlant
* General information on Thassilion
* Mordant Spire
* General information on Casmaron, including geography, history, and current nations
* Pit of Gormuz + Rovagug stuff
* Kelesh, Padishah Empire
* Qadira
* Inner Sea Economy
* Basic info on most countries and races.
* Significant info on most playable races including Azlanti, Garundi, Mwangi, Shoanti, Taldans, Tian, Ulfen, Varisians, Vudrani, Dwarves, Elves, Gnomes, Halflings (which I used to think were half-dwarf), Half-elves, and Half-orcs.
* Jalmeray
* Kaladay
* Sweettalkers, Cecaelias, etc.
* Sarusan
* Taumata
* Minata, the Wandering Isles
* Info on available classes and what they are. Specifically: Adept, Alchemist, Antipaladin, Arcanist, Aristocrat, Barbarian, Bard, Bloodrager, Brawler, Cavalier, Champion, Cleric, Diabolist, Druid, Fighter, Gunslinger, Hierophant, Hunter, Inquisitor, Kineticist, Magus, Medium, Monk, Ninja, Occultist, Oracle, Paladin, Psychic, Ranger, Rogue, Samurai, Shaman, Shifter, Skald, Slayer, Sorcerer, Spritualist, Summoner, Swashbuckler, Trickster, Warpriest, Warrior, Witch, Wizard.
* Some mythic paths
* Crown of the World, its people, and its geography
* Shory Empire and its remnants
* What Ages are
* Yjae
* Kho
* General Garund countries + attractions info
* Detailed info regarding Andoran, Almas, and their interiors. (people, attractions)