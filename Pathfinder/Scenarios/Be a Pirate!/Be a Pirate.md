# Be a Pirate! - Pathfinder v0.5
## Author
HalilAI
## Description:
Yarr!

## Prompt
The Inner Sea is one of the most prosperous places on Golarion. All manner of ships grace its seas, sailing to and from dozens of bustling port towns and the richest parts of numerous countries. The great city Absalom sits proudly right in its metaphorical center, Cheliax and Andoran border it to the north, Taldor and Qadira border it to the east, and Rahadoum, Thuvia, and Osirion comprise its southern shores.
What matters to you isn't the wealth of culture, ideas, or people in Inner Sea however! What matters to you is the wealth itself sailing through it insufficiently defended!
You are ${You are __ . (name, age, race, description, class etc.)}.You are a pirate captain! Your trusty ship ${Name your pirate ship:} is a ${How many masts does your pirate ship have? 3 is generally the norm for the big ones, whereas 2 is also somewhat common. Just write the number.}-mast ${What kind of ship is it? Some options: "ship" for the general case, "brig", "carrack", or "galleon" for some specific options. Just write the word.}.
Today, the waters are calm and the weather is fair. You've been itching for a big haul for some time.
Your lookout yells from the crow's nest "Fat Rahadoumi merchant ship, starboard ahoy captain!"
You take your expensive spying glass out and look to the right side of the ship. Indeed, there is a wide merchant cutter so laden with plunder that its waterline is nearly up to the main deck!

## Memory:
You are a pirate of the Inner Sea.

## Author's Note:
This is a magical world of wonders; use a descriptive language.

## Quests
Remember to put down your character and ship details in Memory for increased consistency.

## Tags
fantasy, pathfinder, pirate, gather your party