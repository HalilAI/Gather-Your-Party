# Bounty Hunter - Pathfinder v0.5
## Author
HalilAI
## Description:
Be a bounty hunter based in Absalom.

## Prompt
The Inner Sea is one of the most prosperous places on Golarion. All manner of ships grace its seas, sailing to and from dozens of bustling port towns and the richest parts of numerous countries. The great city Absalom sits proudly right in its metaphorical center, Cheliax and Andoran border it to the north, Taldor and Qadira border it to the east, and Rahadoum, Thuvia, and Osirion comprise its southern shores.
You are ${You are __ . (name, age, race, description, class etc.)}. You are a new bounty hunter in ${Name your bounty hunter organization:}, and you are particularly skilled in ${you are particularly skilled in "__".}.
Your organization is based on Absalom, and you are given jobs that take you all around the Inner Sea. From bringing criminals to justice to uncovering ancient secrets, near anything can be done for the right price.
Your newest assignment is

## Memory
You are a bounty hunter. Your organization is based on Absalom.

## Author's Note:
This is a magical world of wonders; use a descriptive language.

## Quests
Remember to put down your character and bounty details in Memory for increased consistency.

## Tags
fantasy, pathfinder, bounty hunter, gather your party