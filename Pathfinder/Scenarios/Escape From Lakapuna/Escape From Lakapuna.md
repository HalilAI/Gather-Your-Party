# Escape From Lakapuna - Pathfinder v0.4
## Author
HalilAI
## Description:
Lakapuna is a moving island to the far south of Tian Xi. It's essentially cannibal-death-cult Australia + Hawaii filled with top of crazed sorcerers, cultists, and demons.
It is ALSO one of the only places you can find remains of the ancient Taumata - an ancient empire so powerful and prideful that they are said to have been struck down by a Tian Xi god. Is it true? Who knows... Maybe you will.
-unable:
Lakapuna is part of the southern Rendah Pulu region of the Shifting Isles of Minata. Also, Tian Xi is to your far, far north.

## Prompt
You are Salleri, a young half-elf. You are skilled in tracking, hunting, traps, and archery. You have some minor skills in evocative fire magic. 
Somehow, you find yourself face-down on a tropical beach. You can see swarthy humans wearing bush-clothes in the distance. Through the disorienting fog of your memories, you remember the voyage you had taken to discover the secrets of the ancient Taumata. You remember the storm. You must have been shipwrecked then. That means you are now on Lakapuna, the outwardly beautiful island of cannibals and demon cultists. Great.

//

You are ${Your name is: _.}, a ${Your age and race is: __. (e.g. "young half elf")}. You are skilled in ${You are skilled in __.}.
You find yourself face-down on a tropical beach. You can see swarthy humans wearing bush-clothes in the distance. Through the disorienting fog of your memories, you remember the great voyage you had taken to the southeast of Golarion in order to discover the secrets of the ancient Taumata. You remember the storm. You must have been shipwrecked, then. Which means you are now on Lakapuna, the outwardly beautiful island full of cannibals and demon cultists. Great.

## Memory:
You are Salleri, a young half-elf. You are skilled in tracking, hunting, traps, and archery. You have some minor skills in evocative fire magic. 
Your goal is to discover the secrets of the ancient Taumata, and to escape the hellhole known as Lakapuna.

## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
Discover the secrets of the ancient Taumata.
Escape Lakapuna.

## Tags
fantasy, pathfinder, dnd, rpg, survival, escape, island
