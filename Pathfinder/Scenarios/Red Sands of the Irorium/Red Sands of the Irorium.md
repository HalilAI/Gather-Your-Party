# Red Sands of the Irorium
## Author
HalilAI

## Description (400 keys)
You are a gladiator raised in Absalom.
Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

//

You are a gladiator raised under the Irorium in Absalom, the City at The Center of The World. Choose your destiny, take up your arms, and rip and tear until it is done.

## Prompt (2000 keys)
The sand is warm and welcoming under you. You feel groggy, and memories come rushing back. You are ${You are ___ (use this to input name, age, race, species, etc.)}. Your earliest memories are of being trained in the underground chambers of the Irorium. Even from the earliest of your days, it was obvious that you had a talent for ${You have a talent for __}. It was a good thing, too, since you needed every edge to achieve your lifelong dream to become the greatest warrior Inner Sea has ever seen. Now, someone is literally standing in the way of your dream.

You stand back up and brandish your ${Your preferred weapon is a/an ___}. You are not out of this fight yet. 
The rowdy crowds in the upper rows of the Irorium cheer and boo in turn when they see you stand back up. A handful of other duels are happening in the other rings around you at the base of the Irorium, ringing clashes of metal and the occasional sight of flying blood threatening to distract you. With some effort, you focus back on your own duel.

Your foe stands tall at the other side of the ring in the sand. He is a thickly muscled orc wearing gladiator armor and he wields a trident and a buckler. With a gleeful roar, he sprints towards you.

## Memory: (1000 keys)
You were raised in the Irorium. You are a gladiator. Your goal is to become the best warrior the Inner Sea has ever seen.

## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
Win the current duel.
Become the Champion of the Irorium.
Become the best gladiator in the Inner Sea region.

## Tags
fantasy, pathfinder, dnd, rpg, gladiator, warrior, fight, tournament
