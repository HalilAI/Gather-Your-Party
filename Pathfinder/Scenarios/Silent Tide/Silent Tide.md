# Silent Tide
## Author
HalilAI

## Description (400 keys)
Absalom is about to be invaded from beyond the grave. A group of intrepid Pathfinders is the only thing standing between Nessian and his lantern lights, before the launch of the Silent Tide.

Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

## Prompt (2000 keys)
Your boots squash with fetid water as you plod through the Puddles. A thick fog hangs in the air, enveloping you in an unearthly chill. As you stand in the brackish brine of the half-sunk district, you remember your meeting at the Lodge just a few hours earlier with your Venture-Captain, Adril Hestram.
Adril’s wild beard wags as he examines a scrap of ancient parchment on the table before speaking: “Come in friends; the society is in need. An old and quite penniless historian named Yargos Gill has recently made a very interesting discovery: an ancient codebook, left behind in the wake of one of Taldor’s many failed attempts at invasion. This several ancient text must be added to our collection."
Yargos lives in ‘The Puddles’, Absalom’s poorest district. Following an earthquake ten years ago, parts of the Puddles now rest below sea level, resulting in frequent and untimely flooding. The district is the stomping ground of pimps, harlots, addicts, knifers, and hoards of unseemly derelicts.
"Recently, several persons claim to have seen cloaked, skeletal-like figures marching through an unnatural fog. Tracking down Yargos is now a priority lest some thug cut him down, or one of these strange wraiths carry him beyond the pale. Find him, fellow Pathfinders, and find the codebook. Your exploits will be recorded in the Chronicles if you succeed.”
The memory fades as a fresh deluge of cold seawater assaults your knees. After searching for Yargos at his favorite eatery, the Soggy Piper, you learned you just missed him. According to the Piper’s staff, a gang of dangerous young tattooed toughs arrived ahead of you. They grabbed Yargos and dragged him to a nearby cliff at the edge of the Puddle District. The watch was called, but they will arrive too late, as usual.
It is now early evening, the first Wealday of the month of Desnus, an old man is about to face some awful fate at the sea cliff known as Torsen’s Maw, and you are no closer to finding the codebook Adril sent you for.

## Memory: (1000 keys)
Your goal is to find Yargos Gill and recover the Silent Tide codebook. 
Yargos Gill wants to stop a young crime lord named Nessian from using the Silent Tide codes, raising undead operatives, and destroying Absalom. 
Nessian's thugs are the people who took Yargos.
The group of ancient undead operatives named Black Echelon are controlled by the Silent Tide codes. While you race to find Yargos Gill or Nessian, Black Echelon operatives may tarry and distract you. If you cannot stop Nessian, Absalom will be overrun.

## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
Find Yargos.
Recover the Silent Tide textbook.
Figure out what is going on in the city.

## Tags
fantasy, pathfinder, dnd, rpg, mystery, detective, crime

