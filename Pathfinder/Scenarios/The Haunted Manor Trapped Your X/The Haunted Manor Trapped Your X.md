# The Haunted Manor Trapped Your X - Pathfinder v0.5
## Author
HalilAI
## Description:
Happy halloween!

## Prompt
Golarion is home to many mysterious and wonderful things, as it is home to many unsettling and unnatural ones, and it is your debatable privilege to handle the latter today.
You are ${"You are __." Fill the blank and use this for name, age, profession, class, race, etc.}. Your ${"Your __ has been trapped by the haunted manor" Fill the blank with name, type of relation, etc.} has gone into the Foxglove Manor on the western edge of Kortos. You do not know if ${Pronoun for whomever was kidnapped:} was coerced, paid, or did it in a mad bout of curiosity. Everyone in Absalom knows that Foxglove Manor is cursed or haunted, and has been for generations! There are tales of ghosts, poltergeists, vampires, ghouls, zombies, and many more strange horrors that apparently make their home in the place. The only reason it's still standing is that all demolition efforts have mysteriously stopped; the people in charge either resigned, went on prolonged vacations, inexplicably changed careers, or died. 
You went to as many officials as possible, trying to organize a rescue mission, but it went nowhere. Desperate and running out of time, you went to the one place your mom told you never to go with a request and coin. You went to the only kind of people who would help you in this insane and desperate venture for hard gold and the ephemeral promise of "loot". You even got four of them, as is custom.
Tonight you stand at the narrow cliff's edge leading up to the intimidating Foxglove Manor. A sudden storm is whipping ice-cold rain through your cloak, and dramatic lightning outlines the rushing sea at the base of the tall manor. You wonder what kind of bone-chilling horrors are waiting for you inside Foxglove Manor, and if it will make a difference that your bones are already quite chilly. 
Your four companions follow behind you towards the haunted house and you wonder if the greater madness lies before you or follows after you:
Foxglove Manor... or Adventurers?

## Memory:
You need to save someone from Foxglove Manor.
You are accompanied by four Adventurers. They have agreed to assist you for gold (half of which you paid) and for any possible "loot" to be found in the house. Adventurers are known to be rather excited and foolhardy in their dealings with the supernatural.
Foxglove Manor is a haunted house that houses all manner of dark and horrifying creatures and phenomena. These include twisted reflections of people who got lost in the manor before, vampire scuba divers, horrifying extradimensional structures and creatures, ghouls, ghasts, zombies, ghosts, liches, and more.

There are only two rules rule that Foxglove Manor and its inhabitants must follow: One: They must stay inside the manor. Two: They must be subtle, and they must be spooky.

## Author's Note:
This is a spooky world of unnatural horrors; use a descriptive language.

## Quests
Remember to keep track of your bio particulars (and possibly the Adventurers') in Memory for increased consistency.

## Tags
fantasy, pathfinder, spooky, gather your party