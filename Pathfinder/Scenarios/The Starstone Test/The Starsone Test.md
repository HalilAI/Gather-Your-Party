# The Starstone Test - Pathfinder v0.5
## Author
HalilAI
## Description:
What does it take to be a god?

## Prompt
Absalom is the city at the center of the Inner Sea, which is at the center of Golarion.
You are ${You are "___". (Use this to describe name, age, class, etc.)}. Today you are in Absalom to take the test of the Starstone and become a god. You aim to become the god of ${You aim to become the god of "__".}.
The crowds part before you around the chasm that separates the Starstone Cathedral from the city proper. Some of them are interested, but most are indifferent. Thousands have entered the Starstone Cathedral to become gods, only to die ignoble deaths inside and never leave. Only three have succeeded.  No one knows what tests await inside the Starstone Cathedral. You are about to find out.

## Memory
The Starstone Cathedral is in the middle of Absalom, and it contains the Starstone. The Test of the Starstone is how one becomes a god. Three people in history have become gods this way. No mortal knows what the test actually entails. Is it a mega-dungeon? Is it a test of character, faith, or ambition? You will find out.

## Author's Note:
This is a magical world of wonders; use a descriptive language.

## Quests
Remember to put down your character details in Memory for increased consistency.

## Tags
fantasy, pathfinder, godhood, gather your party