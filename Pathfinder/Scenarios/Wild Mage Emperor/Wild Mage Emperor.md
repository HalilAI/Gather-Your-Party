# Wild Mage Emperor
## Author
HalilAI

## Description (400 keys)
In a world of magic, you can't do normal magic. You can do... Wild Magic! Hopefully your magic won't turn you into a mathematical equation in your journey to be the Mage-Emperor of Golarion.

Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

## Prompt (2000 keys)
You are ${Your name is __.}, a teenaged half-elf. Strange things have been happening around you ever since you were twelve, such as wine turning into cotton or wild animals sitting up to recite history. One of those wild animals, a talking bat you named Batocalypse, has become your trusted familiar. 
The ice-capped Mounts of Kortos have been your home for as long as you have known yourself. You grew up in Eastbarrow, a secluded mining village in the eastern part Kortos. Your drunk father is a miner like most other adults here. Growing up in a close-knit village as the half-elf child of two humans, you grew used to cruel whispers behind your back. 
You need to leave.
You have been gathering information about Absalom, the city at the center of Kortos, from the occasional trader and travelling mage. You learned about the Arcanamirium and College of Mysteries, two great schools of the arcane. If you can gain admission and a scholarship, you can make something great of your life.
Last summer, a Pathfinder named Kilis recognized your bizarre brand of magic as "Wild Magic". He told you that it was weird, unpredictable, and extremely dangerous. He taught you simple exercises to increase your control over your magic. It took you months, but you managed to gain a tiny bit of control over your talents. Strange things still happen when you use magic, but at least they rarely happen spontaneously now.
Your sixteenth birthday does not go so well. Your father accuses you of being a leech and a changeling that stole his actual child. 
Your father kicks you out. After fate's proverbial kick to your butt, it is time for you to go forth and realize your destiny. You will get to Absalom, earn your way into a school of magic, and become the greatest mage emperor Golarion has ever seen!
But first, you need to survive this cold winter night. Batocalypse burrows into your cloak to hide from the cold. You grasp your walking stick and close your rough cloak. Your adventure begins.

## Memory: (1000 keys)
You are a sixteen year old half-elf. Your are penniless.
You cannot use normal magic; instead your talents lie in Wild Magic. The effects of Wild Magic are random and hard to control; ranging from utterly mundane to unthinkably dangerous effects. The greater your magic skill, the more you can control the outcome of your Wild Magic. Experience, study, practice, and knowledge can slowly increase your magical control.
Your current level of magical control is: very low.
Your ultimate desire is to be the mage emperor of Golarion.

You are wearing a rough cloak and thick winter clothes. Your sturdy walking stick is in your hand.

Your trusty familiar Batocalypse is in your cloak. He has some talent in dark magic, and fancies himself your master. He gets stronger along with you. He is still a tiny talking bat, however. 

Your current goal is to survive the cold night and make your way to Absalom. There, you hope to gain free admission to either the Arcanamirium or the College of Mysteries.


## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
Travel to Absalom.
Gain admission to the Arcanamirium, the College of Mysteries, or some other magical education.
Increase your magical control to "Perfect". This will likely take very long.

## Tags
fantasy, pathfinder, dnd, magic, magic school, mage, rpg
