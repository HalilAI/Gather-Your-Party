# Pathfinder Scenario Prompts
# Generate your own custom scenario
## Author
HalilAI
## Description (400 keys)
Using the Pathfinder v0.5 module, create your own adventure!

Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

## Prompt (2000 keys)
Golarion is a planet full of ancient secrets, awesome magics, rich culture, and great adventure. 
You are ${Your name is __}, ${You are __ [a/an age + race]}. You are skilled in ${You are skilled in __}. 
Your journey begins in ${Your journey begins in __ E.g. Absalom, Arcadia, Lakapuna, Jalmeray, etc.}. Your goal is ${Your goal is __}.
${If you have anything else to add to the beginning of the story, do this now.}

## Memory: (1000 keys)
value

## Author's Note:
This is a world filled with magical wonders; use a descriptive writing style.

## Tags
fantasy, pathfinder, dnd, rpg


# Inheritance Re-stolen?
## Description (400 keys)
Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

## Prompt (2000 keys)
value

## Memory: (1000 keys)
value

## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
Put your character's details (bio, goals, motivations, abilities, appearance) in ## Memory.

## Tags
fantasy, pathfinder, dnd, magic, magic school, mage, rpg


# The Wealth of Dragons
## Description (400 keys)
Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

## Prompt (2000 keys)
value

## Memory: (1000 keys)
value

## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
value

## Tags
fantasy, pathfinder, dnd, magic, magic school, mage, rpg


# Name
## Description (400 keys)
Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

## Prompt (2000 keys)
value

## Memory: (1000 keys)
value

## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
value

## Tags
fantasy, pathfinder, dnd, magic, magic school, mage, rpg

## Memory

key
--
value

key
--
value


# Name
## Description (400 keys)
Remember to keep track of ongoing important events (e.g. quests) > motivations > abilities > appearance.

## Prompt (2000 keys)
value

## Memory: (1000 keys)
value

## Author's Note:
This magical world is filled with wonders; use a descriptive writing style.

## Quests
value

## Tags
fantasy, pathfinder, dnd, magic, magic school, mage, rpg