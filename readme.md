# Gather Your Party
## A Cooperative AI Dungeon 2 World Creation Tool

With AI Dungeon recently adding functionality for World Info upload/download, we have a clear path to creating intricate and rich Scenarios without having to copy and paste for hours. That's great!

As it is, this repository includes some of my scenarios that particularly benefit from significant World Info. This includes Pathfinder and Worm, the former of which has more than 30,000 words of WI the last time I counted. The Pathfinder scenario even started giving me lore that I initially thought was a bug but *later* learned was true. That meant that I could learn new things in a world I had written in that were actually true and coherent. 

That, in my book, is a success.

## Contribution

Please feel free to contribute to existing Worlds / Scenarios or creating new ones from whole cloth! This repository is for us to be collective world builders rather than ones limited by our solitude.

Help with organizing the repository, making the readme's pretty, and organizing the text files are all welcome. I have also started making a plug-and-play auto updater for everyone, but it isn't functional yet. If anyone is interested in helping, we will work on it in this repository and use Python + Selenium WebDriver. 

For now, when your changes are integrated into the main branch, the Scenario uploader needs to update them accordingly. Thankfully, that is now a much easier proposition than it used to be! Just to be clear, the data in this repository is currently intended to be used through manual upload (which takes one click on AID).

Please don't break formatting in the JSON files. It's a bit awkward, so watch out. Also, watch out for the dash count in the raw text file entries. They are important for preserving a useful uniform format. You'll understand when you see it. Just copy and paste for a new entry.

When you use significant World Info, please read the relevant posts on Ai Dungeon or the Discord server. There are lots of good tips there that can make the difference between a terrible experience and a great one. 

For most Worlds, do *not* make a World Info key for things like "you", "", " ", "I", "a", etc. If you must follow one rule, it should be to make your World Info keys as specific as possible. Any more general keys should be used sparingly.

If you are adding to an existing World Info file, make sure to search first! Do not add information that already exists.

One big question we need to answer is how to handle Scenarios that need additional World Info when we need to update them. Let me know what your ideas are!

## Structure

In each World folder you will find four things: A "JSON" folder, a "Text" folder, and two .md files (e.g. Progress.md and Pathfinder.md). The JSON folder includes the current World Info file that will be kept in sync with the AI Dungeon upload. The Text folder includes raw text and notes about the World. The world-specific .md file includes the information you would normally input as you edit a scenario on Ai Dungeon. Finally, the Progress.md file includes information on what the (possibly gigantic) World Info file includes and what is planned to be added. 

Please note your changes in the Progress.md files as appropriate.

Scenarios made using Worlds will have their own folders with Scenario information .md files under their "parent" World folders. Using that structure, you can make use of the World Info to make your own Scenarios and share your new Scenarios freely on AID. Please mention Gather Your Party if you do so! 

The Scenarios created using Worlds are given version numbers indicating which World version they were based off of. For example, Red Sands of the Irorium v0.4 was made by cloning Pathfinder - Base Module v0.4 and adding Scenario-specific information.

---

### Current state of AYP: 
1. Uploading scenarios to the repository and formatting them.
2. Working on automating headless JSON uploads.

---

# Worlds:
## Pathfinder
It's Pathfinder. A setting generously inspired by real-life cultures and steeped in interesting things, you can't go wrong with it.
Please help write a synopsis.
### Scenarios:
* Generate Your Own Scenario v0.4
* Red Sands of the Irorium v0.4
* 

## Worm
Superheroes but insane. For the low low price of getting an alien supercomputer make a USB port in your brain, you can become one!
### Scenarios: